const books = [
    { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
}, 
    {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
}, 
    { 
    name: "Дизайн. Книга для недизайнерів",
    price: 70
}, 
    { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
}, 
    {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
},
    {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
}
];

let root = document.getElementById('root');
let list = document.createElement('ol');
list.classList.add('list-of-books');
root.append(list)

// Виведіть цей масив на екран у вигляді списку 
// (тег ul – список має бути згенерований за допомогою Javascript)

for (let book of books) {

    // Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність 
    // (в об'єкті повинні міститися всі три властивості - author, name, price). 
    // Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із 
    // зазначенням - якої властивості немає в об'єкті. 
    // Ті елементи масиву, які не є коректними за умовами попереднього пункту, 
    // не повинні з'явитися на сторінці.
    try {
        if ("author" in book && "name" in book && "price" in book) {
            let listItem = document.createElement('li');
            let nestedList = document.createElement('ul');
            nestedList.classList.add('book-info');
            listItem.append(nestedList);
    
            for (let item in book){
                let bookInfo = document.createElement('li');
                bookInfo.textContent = `${item} - ${book[item]};`;
                nestedList.append(bookInfo);
            }
    
            list.append(listItem);
        }   

        else {
            if(!Object.keys(book).includes("author")){
                    throw new Error(`Information about author of a book "${book.name}" is missing!`);
                }

            else if(!Object.keys(book).includes("name")){
                    throw new Error(`Information about name of a book of ${book.author} is missing!`)
                }
            else if(!Object.keys(book).includes("price")){
                    throw new Error(`Information about price of a book "${book.name}" is missing!`)
                }
            }

    } 
    catch (error) {
        console.error('Error with book info: ', error.message);
    }
}